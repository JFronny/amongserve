﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;
using HarmonyLib;
using Impostor.Api;
using Impostor.Api.Events.Managers;
using Impostor.Api.Plugins;
using Microsoft.Extensions.Logging;

namespace ImpostorLoader
{
    [ImpostorPlugin(Package, Name, "JFronny", "1.0.0")]
    public class PluginMain : PluginBase
    {
        private const string Package = "io.gitlab.jfronny";
        private const string Name = "ImpostorLoader";
        private const string Target = "Impostor.Server";

        private static readonly string[] IgnoredMethods =
        {
            "virtual System.Threading.Tasks.ValueTask Impostor.Server.Net.Inner.Objects.InnerLobbyBehaviour::HandleRpc(Impostor.Server.Net.State.ClientPlayer sender, Impostor.Server.Net.State.ClientPlayer target, Impostor.Server.Net.Inner.RpcCalls call, Impostor.Api.Net.Messages.IMessageReader reader)"
        };

        private static ILogger<PluginMain> _logger;
        public override ValueTask EnableAsync() => default;
        public override ValueTask DisableAsync() => default;

        public PluginMain(ILogger<PluginMain> logger, IEventManager eventManager)
        {
            _logger = logger;
            Harmony harmony = new Harmony(Package + "." + Name);
            Assembly asm = AppDomain.CurrentDomain.GetAssemblies()
                .SingleOrDefault(s => s.GetName().Name == Target);
            if (asm == null)
            {
                logger.LogError("Could not find server assembly, quitting");
                return;
            }
            HarmonyMethod transpiler = new HarmonyMethod(SymbolExtensions.GetMethodInfo(() => Transpiler(null, null)));
            foreach (Type type in asm.GetTypes())
                if (!type.IsGenericType && type.FullName.StartsWith(Target))
                {
                    MethodInfo[] methodInfos = type.GetMethods();
                    foreach (MethodInfo method in methodInfos)
                    {
                        string name = method.FullDescription();
                        if (!method.DeclaringType.FullName.StartsWith(Target))
                        {
                        }
                        else if (IgnoredMethods.Contains(name))
                        {
                        }
                        else if ((method.Attributes & MethodAttributes.Abstract) != 0)
                        {
                        }
                        else if (method.IsGenericMethod)
                        {
                            logger.LogInformation("Skipping " + name + " as that is generic");
                        }
                        else if (!method.IsDeclaredMember())
                        {
                            logger.LogInformation("Skipping " + name + " as that is not declared");
                        }
                        else
                        {
                            logger.LogInformation("Patching " + name);
                            //logger.LogInformation(method.Attributes.ToString());
                            harmony.Patch(method, transpiler: transpiler);
                        }
                    }
                }
        }
        
        // ReSharper disable once UnusedMethodReturnValue.Local
        [HarmonyTranspiler]
        private static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions , MethodBase original)
        {
            string methodDescriptor = original.FullDescription();
            List<CodeInstruction> il = new List<CodeInstruction>(instructions);
            for (int i = 0; i < il.Count; i++)
            {
                if (il[i].opcode == OpCodes.Throw)
                {
                    CodeInstruction instructionPrev = il[i - 1];
                    if (instructionPrev.opcode == OpCodes.Newobj)
                    {
                        MethodBase method = instructionPrev.operand as MethodBase;
                        if (method.DeclaringType == typeof(ImpostorCheatException))
                        {
                            _logger.LogInformation("Found " + nameof(ImpostorCheatException) + ", disabling throw");
                            il[i] = new CodeInstruction(OpCodes.Pop);
                        }
                    }
                    else
                    {
                        _logger.LogWarning("Throw not preceded by newobj in " + methodDescriptor);
                        il[i] = new CodeInstruction(OpCodes.Pop);
                    }
                }
            }
            return il;
        }
    }
}