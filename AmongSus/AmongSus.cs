﻿using AmongServe.WindowSystem;
using AmongSus.Windows;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.IL2CPP;
using BepInEx.Logging;
using HarmonyLib;
using Reactor;
//TODO fix the messed-up pause menu
namespace AmongSus
{
    [BepInPlugin(Id)]
    [BepInProcess("Among Us.exe")]
    [BepInDependency(ReactorPlugin.Id)]
    [ReactorPluginSide(PluginSide.ClientOnly)]
    public class AmongSus : BasePlugin
    {
        public const string Id = "io.gitlab.jfronny.AmongSus";
        public static AmongSus Instance;
        public Harmony Harmony { get; } = new(Id);
        internal static ManualLogSource Logger;
        public readonly Window[] Windows =
        {
            new StateWindow(),
            new HelpWindow(),
            new TeleportWindow(),
            new TweaksWindow(),
        };
        public ConfigEntry<bool> WindowVisible { get; private set; }

        public override void Load()
        {
            Logger = Log;
            Instance = this;
            WindowVisible = Config.Bind("General", nameof(WindowVisible), true);
            RegisterInIl2CppAttribute.Register();
            foreach (Window window in Windows)
                window.Register(new InitProvider(window.GetName(), Config));
            
            Harmony.PatchAll();
        }
    }
}
