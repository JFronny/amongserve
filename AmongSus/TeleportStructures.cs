﻿using System.Collections.Generic;

namespace AmongSus
{
    public class Teleports
    {
        public Teleports(byte mapId, string mapName)
        {
            MapId = mapId;
            MapName = mapName;
        }

        public byte MapId { get; }
        public string MapName { get; }
        public List<Teleport> Locations { get; } = new();
    }

    public class Teleport
    {
        public Teleport(string name, float x, float y)
        {
            Name = name;
            X = x;
            Y = y;
        }

        public string Name { get; }
        public float X { get; }
        public float Y { get; }
    }
}