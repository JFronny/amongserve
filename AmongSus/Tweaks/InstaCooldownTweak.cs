﻿namespace AmongSus.Tweaks
{
    public class InstaCooldownTweak : Tweak
    {
        protected override string ID => "instaCooldown";
        protected override string Name => "Insta-Cooldown";
        public override void Update()
        {
            base.Update();
            if (PlayerControl.LocalPlayer && Enabled)
                PlayerControl.LocalPlayer.SetKillTimer(0);
        }
    }
}