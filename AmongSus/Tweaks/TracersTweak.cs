﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmongServe.WindowSystem;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongSus.Tweaks
{
    public class TracersTweak : Tweak
    {
        protected override string ID => "tracers";
        protected override string Name => "Tracers";
        private List<LineDrawer> _tracerDrawers;

        public override void Init(InitProvider init)
        {
            base.Init(init);
            _tracerDrawers = new List<LineDrawer>();
        }

        public override void DrawContent(Func<Il2CppReferenceArray<GUILayoutOption>> g)
        {
            base.DrawContent(g);
            if (PlayerControl.LocalPlayer)
            {
                PlayerControl[] list = PlayerControl.AllPlayerControls.ToArray().ToArray();
                if (Enabled)
                {
                    while (_tracerDrawers.Count < list.Length)
                        _tracerDrawers.Add(new LineDrawer(0.01f));
                    while (_tracerDrawers.Count > list.Length)
                    {
                        _tracerDrawers[0].Destroy();
                        _tracerDrawers.RemoveAt(0);
                    }
                }
                else
                    ClearDrawers();
                Vector3 position = PlayerControl.LocalPlayer.transform.position;
                for (int i = 0; i < list.Length; i++)
                {
                    PlayerControl control = list[i];
                    if (Enabled)
                        _tracerDrawers[i].DrawLineInGameView(position, control.transform.position,
                            control.Data.IsDead ? Color.white : control.Data.IsImpostor ? Color.red : Color.green);
                }
            }
            else
                ClearDrawers();
        }

        private void ClearDrawers()
        {
            while (_tracerDrawers.Count > 0)
            {
                _tracerDrawers[^1].Destroy();
                _tracerDrawers.RemoveAt(_tracerDrawers.Count - 1);
            }
        }
    }
}