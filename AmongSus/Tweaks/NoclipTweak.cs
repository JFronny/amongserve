﻿using UnityEngine;

namespace AmongSus.Tweaks
{
    public class NoclipTweak : Tweak
    {
        protected override string ID => "noclip";
        protected override string Name => "NoClip";
        private const float Speed = 0.5f;

        public override void Update()
        {
            base.Update();
            if (Input.GetKeyDown(KeyCode.Keypad5))
                Enabled = !Enabled;
            /*if (Enabled)
            {
                Vector3 v = PlayerControl.LocalPlayer.transform.position;
                if (Input.GetKey(KeyCode.W)) v.y -= Speed;
                if (Input.GetKey(KeyCode.A)) v.x -= Speed;
                if (Input.GetKey(KeyCode.S)) v.y += Speed;
                if (Input.GetKey(KeyCode.D)) v.x += Speed;
                PlayerControl.LocalPlayer.transform.position = v;
            }*/
            PlayerControl.LocalPlayer.GetComponent<Collider2D>().enabled = Enabled;
            //TODO fix disabling collider using unity explorer once that works again
        }
    }
}