﻿using System;
using AmongServe.WindowSystem;
using BepInEx.Configuration;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongSus.Tweaks
{
    public abstract class Tweak
    {
        private ConfigEntry<bool> _entry;
        protected abstract string ID { get; }
        protected abstract string Name { get; }

        public virtual void Init(InitProvider init)
        {
            _entry = init.RegisterConfig(ID, false);
        }

        public virtual void DrawContent(Func<Il2CppReferenceArray<GUILayoutOption>> g)
        {
            _entry.RenderBox(Name);
        }

        public virtual void Update()
        {
        }

        public bool Enabled
        {
            get => _entry.Value;
            set => _entry.Value = value;
        }
    }
}