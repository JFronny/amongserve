﻿using AmongServe.WindowSystem;
using BepInEx.Configuration;
using UnityEngine;

namespace AmongSus.Tweaks
{
    public class SpeedTweak : Tweak
    {
        private float _fixedDeltaTime;
        private ConfigEntry<int> _speedAmount; //TODO add UI (slider)
        protected override string ID => "speed";
        protected override string Name => "Speed";
        public override void Update()
        {
            base.Update();
            if (Input.GetKeyDown(KeyCode.Keypad4)) Enabled = !Enabled;
            Time.timeScale = Enabled ? _speedAmount.Value : 1;
            Time.fixedDeltaTime = _fixedDeltaTime * Time.timeScale;
        }

        public override void Init(InitProvider init)
        {
            base.Init(init);
            _fixedDeltaTime = Time.fixedDeltaTime;
            _speedAmount = init.RegisterConfig("speedAmount", 3);
        }
    }
}