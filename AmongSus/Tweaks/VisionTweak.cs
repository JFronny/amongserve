﻿namespace AmongSus.Tweaks
{
    public class VisionTweak : Tweak
    {
        protected override string ID => "vision";
        protected override string Name => "Increased vision";
        public override void Update()
        {
            base.Update();
            if (PlayerControl.GameOptions != null && Enabled)
            {
                PlayerControl.GameOptions.CrewLightMod = 5;
                PlayerControl.GameOptions.ImpostorLightMod = 5;
            }
        }
    }
}