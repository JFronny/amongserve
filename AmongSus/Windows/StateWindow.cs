﻿using System;
using System.Linq;
using AmongServe.WindowSystem;
using BepInEx.Configuration;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongSus.Windows
{
    internal class StateWindow : Window
    {
        private ConfigEntry<ForceState> _forceState;
        private const string Force = "Force";
        private const string Impostor = "Impostor";
        private const string Crew = "Crew";
        private const string Dead = "Dead";
        public override void DrawContent(Func<Il2CppReferenceArray<GUILayoutOption>> g)
        {
            if (_forceState.Value != ForceState.None)
                _forceState.Value = (_forceState.Value == ForceState.Impostor).RenderBox(Impostor)
                    ? ForceState.Impostor
                    : ForceState.Crew;
            if (PlayerControl.LocalPlayer)
            {
                bool val1 = PlayerControl.LocalPlayer.Data.IsDead;
                bool val2 = val1.RenderBox(Dead);
                if (val1 != val2)
                {
                    if (val2)
                        PlayerControl.LocalPlayer.MurderPlayer(PlayerControl.LocalPlayer);
                    PlayerControl.LocalPlayer.Data.IsDead = val2;
                }
                if (_forceState.Value == ForceState.None)
                {
                    val1 = PlayerControl.LocalPlayer.Data.IsImpostor;
                    val2 = val1.RenderBox(Impostor);
                    if (val1 != val2)
                        PlayerControl.LocalPlayer.Data.IsImpostor = val2;
                }
                else
                    PlayerControl.LocalPlayer.Data.IsImpostor = _forceState.Value == ForceState.Impostor;
                FSel();
                Vector3 position = PlayerControl.LocalPlayer.transform.position;
                GUILayout.Label($"X: {position.x} - Y: {position.y}", g());
                PlayerControl[] list = PlayerControl.AllPlayerControls.ToArray().ToArray();
                foreach (PlayerControl control in list)
                {
                    Vector3 p = control.transform.position;
                    if (!control.Data.IsDead)
                        GUILayout.Label($"{control.name} - X: {p.x} - Y: {p.y} " + (control.Data.IsImpostor ? Impostor : Crew), g());
                }
            }
            else
            {
                FSel();
                GUILayout.Label("Not in game", g());
            }
        }
        
        private void FSel() => _forceState.Value = _forceState.Value == ForceState.None ? 
            false.RenderBox(Force) ? PlayerControl.LocalPlayer && PlayerControl.LocalPlayer.Data.IsImpostor ? ForceState.Impostor : ForceState.Crew : ForceState.None
            : true.RenderBox(Force) ? _forceState.Value : ForceState.None;

        private enum ForceState
        {
            None,
            Crew,
            Impostor
        }

        public override string GetName() => "State";

        protected override void Setup(InitProvider init)
        {
            _forceState = init.RegisterConfig(nameof(_forceState), ForceState.None);
        }

        public override void Update()
        {
            if (Input.GetKeyDown(KeyCode.Keypad7))
            {
                if (_forceState.Value == ForceState.Crew)
                    _forceState.Value = ForceState.Impostor;
                else if (_forceState.Value == ForceState.Impostor)
                    _forceState.Value = ForceState.Crew;
                else
                    PlayerControl.LocalPlayer.Data.IsImpostor = !PlayerControl.LocalPlayer.Data.IsImpostor;
            }
            if (Input.GetKeyDown(KeyCode.End))
            {
                _forceState.Value = ForceState.None;
            }
        }
    }
}
