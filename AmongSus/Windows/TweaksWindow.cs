﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmongServe.WindowSystem;
using AmongSus.Tweaks;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongSus.Windows
{
    public class TweaksWindow : Window
    {
        private static readonly List<Tweak> Tweaks = new();
        
        public override string GetName() => "Tweaks";

        protected override void Setup(InitProvider init)
        {
            Tweaks.AddRange(AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.Modules)
                .SelectMany(s => s.GetTypes())
                .Where(s => s.IsClass && !s.IsAbstract)
                .Where(s => typeof(Tweak).IsAssignableFrom(s))
                .Select(s => (Tweak)Activator.CreateInstance(s))
            );
            foreach (Tweak tweak in Tweaks) tweak.Init(init);
        }

        public override void DrawContent(Func<Il2CppReferenceArray<GUILayoutOption>> g)
        {
            foreach (Tweak tweak in Tweaks) tweak.DrawContent(g);
        }

        public override void Update()
        {
            if (Input.GetKeyDown(KeyCode.End))
                foreach (Tweak tweak in Tweaks) tweak.Enabled = false;
            foreach (Tweak tweak in Tweaks) tweak.Update();
        }

        public static bool GetTweakEnabled<T>() where T : Tweak
        {
            foreach (Tweak tweak in Tweaks)
                if (tweak is T)
                    return tweak.Enabled;
            throw new IndexOutOfRangeException();
        }
    }
}
