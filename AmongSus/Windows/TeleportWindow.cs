﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmongServe.WindowSystem;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongSus.Windows
{
    public class TeleportWindow : Window
    {
        private readonly List<Teleports> _teleports = new()
        {
            new(0, "The Skeld")
            {
                Locations =
                {
                    new("Med Bay", -9.177800179f, -2.285377741f),
                    new("Report Table", -0.8012289405f, 2.12445569f),
                    new("Camera Room", -12.92269421f, -3.656251669f),
                    new("Reactor", -20.58441544f, -5.441064358f),
                    new("Electric Room", -6.85087347f, -8.838123322f),
                    new("Storage Room", -4.249099255f, -13.35535336f),
                    new("Admin Room", 5.10692215f, -8.005049706f),
                    new("Engine Room", -17.0474205f, -9.72734642f),
                    new("SPACE", -26.81193352f, -8.841944695f),
                }
            },
            new(1, "MIRA HQ")
            {
                Locations =
                {
                    new("Report Table", 25.08257675f, 1.374570012f),
                    new("Storage Room", 18.45582581f, 3.565962791f),
                    new("Satellite", 19.9529686f, -2.586984873f),
                    new("Hallway Center", 17.7395134f, 11.16385651f),
                    new("Admin Room", 19.579216f, 18.58133507f),
                    new("Office", 14.98454189f, 18.19694328f),
                    new("Communication", 14.96714115f, 3.946860552f),
                    new("Locker Room", 10.62946129f, 3.722297907f),
                    new("Med Bay", 14.22113705f, -0.1889319718f),
                    new("Launch Pad", -4.008491993f, 0.1534152925f),
                    new("SPACE", 26.20145226f, 7.394963741f),
                }
            },
            new(2, "Polus")
            {
                Locations =
                {
                    new("Report Table", 19.88724518f, -16.61142921f),
                    new("Admin Room", 23.05901146f, -22.26737022f),
                    new("Office", 27.0331955f, -17.17744637f),
                    new("MIDDLE OF LAVA!", 34.07090378f, -16.13401794f),
                    new("Laboratory", 26.65434837f, -9.835519791f),
                    new("Storage", 20.31784248f, -11.88982487f),
                    new("Electrical", 9.249729156f, -12.49947834f),
                    new("Oxygen / 02 Room", 2.706996441f, -17.39266586f),
                    new("Boiler Room", 2.526514292f, -23.75616837f),
                    new("Weapons Room", 12.75616932f, -23.04491615f),
                    new("Communications", 12.13513374f, -16.81485939f),
                    new("Out Of Map", 3.229459524f, -32.23437881f),
                }
            },
        };
        public override string GetName() => "Teleport";

        protected override void Setup(InitProvider init)
        {
        }

        public override void DrawContent(Func<Il2CppReferenceArray<GUILayoutOption>> g)
        {
            if (PlayerControl.LocalPlayer && PlayerControl.GameOptions != null)
            {
                Vector3 position = PlayerControl.LocalPlayer.transform.position;
                byte map = PlayerControl.GameOptions.MapId;
                if (_teleports.Any(s => s.MapId == map))
                {
                    Teleports teleports = _teleports.First(s => s.MapId == map);
                    GUILayout.Label(teleports.MapName, g());
                    foreach (Teleport teleport in teleports.Locations)
                        if (GUILayout.Button(teleport.Name, g()))
                        {
                            position.x = teleport.X;
                            position.y = teleport.Y;
                        }
                }
                else
                    GUILayout.Label("Unsupported map", g());
                PlayerControl.LocalPlayer.transform.position = position;
            }
            else
                GUILayout.Label("Not in game", g());
        }

        public override void Update()
        {
        }
    }
}