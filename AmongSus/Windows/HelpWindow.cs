﻿using System;
using AmongServe.WindowSystem;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongSus.Windows
{
    internal class HelpWindow : Window
    {
        public override void DrawContent(Func<Il2CppReferenceArray<GUILayoutOption>> g)
        {
            GUILayout.Label("INSERT: Toggle menu visibility", g());
            GUILayout.Label("END: Hide Menu and disable/reset cheats", g());
            GUILayout.Label("NUM4: Toggle speed hack", g());
            GUILayout.Label("NUM5: Toggle Noclip", g());
            GUILayout.Label("NUM7: Toggle mode (crew/impostor)", g());
        }

        public override string GetName() => "Help";

        protected override void Setup(InitProvider init)
        {
            
        }

        public override void Update()
        {
            if (Input.GetKeyDown(KeyCode.Insert)) AmongSus.Instance.WindowVisible.Value = !AmongSus.Instance.WindowVisible.Value;
            if (Input.GetKeyDown(KeyCode.End)) AmongSus.Instance.WindowVisible.Value = false;
            foreach (Window window in AmongSus.Instance.Windows) window.SetEnabled(AmongSus.Instance.WindowVisible.Value);
        }
    }
}
