﻿using System.Collections.Generic;
using AmongSus.Tweaks;
using AmongSus.Windows;
using HarmonyLib;
using UnityEngine;

namespace AmongSus.Patches
{
    [HarmonyPatch(typeof(MapBehaviour), nameof(MapBehaviour.ShowNormalMap))]
    public static class MapBehaviourShowNormalMapPatch 
    {
        public static void Postfix(MapBehaviour __instance) {
            if (!ShipStatus.Instance)
                return;

            if (MiniMapPlayers.Enabled) {
                MiniMapPlayers.ClearAllPlayers();
                __instance.ColorControl.SetColor(new Color(0.894f, 0f, 1f, 1f));

                MiniMapPlayers.HerePoints.Clear();
                foreach (PlayerControl player in PlayerControl.AllPlayerControls) {
                    if (!player.Data.IsDead) {				
                        SpriteRenderer herePoint = Object.Instantiate(__instance.HerePoint, __instance.HerePoint.transform.parent);

                        player.SetPlayerMaterialColors(herePoint);
                        TextRenderer text = Object.Instantiate(HudManager.Instance.TaskText, __instance.HerePoint.transform.parent);
                        text.Text = player.Data.PlayerName;
                        text.Color = player.Data.IsImpostor ? Color.red : Color.white;
                        text.transform.SetParent(herePoint.transform);
                        text.transform.position = herePoint.transform.position;
                        text.transform.localScale = herePoint.transform.localScale;
                        text.Centered = true;
                        MiniMapPlayers.TextRenderers.Add(text);
                        MiniMapPlayers.HerePoints.Add(herePoint);
                    }
                }
            }
        }
    }

	[HarmonyPatch(typeof(MapBehaviour), nameof(MapBehaviour.FixedUpdate))]
	public static class MapBehaviourFixedUpdatePatch {
		public static void Postfix(MapBehaviour __instance) {
			if (!ShipStatus.Instance) { return; }

			if (MiniMapPlayers.Enabled) {
				for (int i = 0; i < MiniMapPlayers.HerePoints.Count; i++) {
					if (!PlayerControl.AllPlayerControls[i].Data.IsDead) {
						Vector3 vector = PlayerControl.AllPlayerControls[i].transform.position;
						vector /= ShipStatus.Instance.MapScale;
						vector.x *= Mathf.Sign(ShipStatus.Instance.transform.localScale.x);
						vector.z = -1f;
						MiniMapPlayers.HerePoints[i].transform.localPosition = vector;
                        MiniMapPlayers.TextRenderers[i].transform.position = MiniMapPlayers.HerePoints[i].transform.position + new Vector3(0, 0.3f, 0);
                        MiniMapPlayers.TextRenderers[i].Text = PlayerControl.AllPlayerControls[i].Data.PlayerName;
					}	
				}
			}
		}
	}

	[HarmonyPatch(typeof(MapBehaviour), nameof(MapBehaviour.Close))]
	public static class MapBehaviourClosePatch
    {
		public static void Postfix(MapBehaviour __instance) => MiniMapPlayers.ClearAllPlayers();
    }
    
    [HarmonyPatch(typeof(ShipStatus), nameof(ShipStatus.Start))]
    internal class GameEndedPatch
    {
        public static void Postfix(ShipStatus __instance)
        {
            MiniMapPlayers.TextRenderers.Clear();
            MiniMapPlayers.HerePoints.Clear();
        }
    }

	public static class MiniMapPlayers
    {
        public static readonly List<SpriteRenderer> HerePoints = new();
        public static readonly List<TextRenderer> TextRenderers = new();
		public static void ClearAllPlayers()
        {
            try {
                HerePoints.ForEach(x => Object.Destroy(x.gameObject));
                TextRenderers.ForEach(x => Object.Destroy(x.gameObject));
                HerePoints.Clear();
                TextRenderers.Clear();
            } catch {}
		}

        public static bool Enabled => TweaksWindow.GetTweakEnabled<BetterMinimapTweak>();
    }
}