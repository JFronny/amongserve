﻿using UnityEngine;

namespace AmongSus
{
    internal class LineDrawer
    {
        private LineRenderer _lineRenderer;
        private readonly float _lineSize;

        public LineDrawer(float lineSize)
        {
            GameObject lineObj = new("LineObj");
            _lineRenderer = lineObj.AddComponent<LineRenderer>();
            //Particles/Additive
            _lineRenderer.material = new Material(Shader.Find("Hidden/Internal-Colored"));

            _lineSize = lineSize;
        }

        private void Init()
        {
            if (_lineRenderer == null)
            {
                GameObject lineObj = new("LineObj");
                _lineRenderer = lineObj.AddComponent<LineRenderer>();
                //Particles/Additive
                _lineRenderer.material = new Material(Shader.Find("Hidden/Internal-Colored"));
            }
        }

        //Draws lines through the provided vertices
        public void DrawLineInGameView(Vector3 start, Vector3 end, Color color)
        {
            if (_lineRenderer == null) Init();

            //Set color
            _lineRenderer.startColor = color;
            _lineRenderer.endColor = color;

            //Set width
            _lineRenderer.startWidth = _lineSize;
            _lineRenderer.endWidth = _lineSize;

            //Set line count which is 2
            _lineRenderer.positionCount = 2;

            //Set the postion of both two lines
            _lineRenderer.SetPosition(0, start);
            _lineRenderer.SetPosition(1, end);
        }

        public void Destroy()
        {
            if (_lineRenderer != null) Object.Destroy(_lineRenderer.gameObject);
        }
    }
}
