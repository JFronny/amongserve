﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace AmongServe.ServerPicker
{
    public class ServerPickerData
    {
        private static List<CustomServerInfo> _customServers = new List<CustomServerInfo>();

        public static readonly IRegionInfo[] DefaultRegions = new IRegionInfo[3];

        public static bool FirstRun = true;
        public static bool ForceReloadServers = false;
        private const string UserDataPath = "UserData";
        private const string CustomServersFileName = "CustomServers.json";
        public static readonly string CustomServersFile = Path.Combine(UserDataPath, CustomServersFileName);
        public const string ManageServers = "MANAGE_SERVERS";

        public static List<CustomServerInfo> CustomServers
        {
            get => _customServers ??= new List<CustomServerInfo>();
            private set => _customServers = value;
        }

        public static void Load()
        {
            Directory.CreateDirectory(UserDataPath);
            if (File.Exists(CustomServersFile))
            {
                CustomServers = JsonConvert.DeserializeObject<List<CustomServerInfo>>(File.ReadAllText(CustomServersFile));
                AmongServe.Logger.LogDebug("Loaded custom servers list from file!");
            }
            else
            {
                AmongServe.Logger.LogWarning("Custom servers list file not found!");
            }
        }

        public static void Save()
        {
            File.WriteAllText(CustomServersFile, JsonConvert.SerializeObject(CustomServers));
        }
    }
}