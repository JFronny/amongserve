﻿namespace AmongServe.ServerPicker
{
    public class CustomServerInfo
    {
        public string Name;
        public string Ip;
        public int Port;

        public CustomServerInfo(string name, string ip, int port)
        {
            Name = name;
            Ip = ip;
            Port = port;
        }

        public override string ToString() => $"{Name} ({Ip}:{Port})";
    }
}