﻿//TODO update once https://github.com/CrowdedMods/CustomServersClient is on 2021
/*
using HarmonyLib;
using UnhollowerBaseLib;
using UnityEngine.UI;
using static AmongServe.ServerPicker.ServerPickerData;
using RegionMenuButtonCallback = RegionMenu.Nested_0;
namespace AmongServe.ServerPicker
{
    [HarmonyPatch(typeof(RegionMenu), nameof(RegionMenu.OnEnable))]
    public static class RegionMenuOnEnablePatch
    {
        public static bool Prefix(ref RegionMenu __instance)
        {
            ClearOnClickAction(__instance.ButtonPool);

            if (FirstRun)
            {
                for (int i = 0; i < 3; i++) DefaultRegions[i] = ServerManager.DefaultRegions[i];
                FirstRun = false;
            }

            Load();

            if (ServerManager.DefaultRegions.Count != 4 + CustomServers.Count || ForceReloadServers)
            {
                RegionInfo[] regions = new RegionInfo[4 + CustomServers.Count];

                regions[0] = new RegionInfo("Manage servers...", ManageServers, null);

                for (int i = 0; i < 3; i++)
                {
                    regions[i + 1] = DefaultRegions[i];
                }

                for (int i = 0; i < CustomServers.Count; i++)
                {
                    Il2CppReferenceArray<ServerInfo> servers = new ServerInfo[1]
                        {new ServerInfo(CustomServers[i].Name, CustomServers[i].Ip, (ushort) CustomServers[i].Port)};

                    regions[i + 4] = new RegionInfo(CustomServers[i].Name, "0", servers);
                }

                ServerManager.DefaultRegions = regions;
            }

            return true;
        }

        public static void ClearOnClickAction(ObjectPoolBehavior buttonPool)
        {
            foreach (CCLDIACANAF button in buttonPool.activeChildren)
            {
                PassiveButton buttonComponent = button.GetComponent<PassiveButton>();
                if (buttonComponent != null)
                    buttonComponent.OnClick = new Button.ButtonClickedEvent();
            }

            foreach (CCLDIACANAF button in buttonPool.inactiveChildren)
            {
                PassiveButton buttonComponent = button.GetComponent<PassiveButton>();
                if (buttonComponent != null)
                    buttonComponent.OnClick = new Button.ButtonClickedEvent();
            }
        }
    }


    [HarmonyPatch(typeof(RegionMenuButtonCallback), nameof(RegionMenuButtonCallback.Method_Internal_Void_0))]
    public static class RegionMenuChooseOptionPatch
    {
        public static bool Prefix(ref RegionMenuButtonCallback __instance)
        {
            if (__instance.region.PingServer == ManageServers)
            {
                AmongServe.ServerManagementWindow.RegionMenu = __instance.__this;
                AmongServe.ServerManagementWindow.SetEnabled(true);
                return false;
            }
            return true;
        }
    }
}
*/