﻿using System;
using System.Diagnostics;
using AmongServe.WindowSystem;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongServe.ServerPicker
{
    //TODO use proper GUI Text input
    public class ServerManagementWindow : Window
    {
        private int _currentId = -1;
        public RegionMenu RegionMenu;

        public override string GetName() => "Server Management";

        protected override void Setup(InitProvider init)
        {
            SetEnabled(false);
        }

        public override void DrawContent(Func<Il2CppReferenceArray<GUILayoutOption>> g)
        {
            if (_currentId == -1)
            {
                for (int i = 0; i < ServerPickerData.CustomServers.Count; i++)
                    if (GUILayout.Button(ServerPickerData.CustomServers[i].ToString(), g()))
                        _currentId = i;
                //GUILayout.Space(10);
                if (GUILayout.Button("Add", g()))
                {
                    ServerPickerData.CustomServers.Add(new CustomServerInfo("New server", "", 0));
                    _currentId = ServerPickerData.CustomServers.Count - 1;
                }
                if (GUILayout.Button("Edit File", g()))
                {
                    Process.Start("notepad", ServerPickerData.CustomServersFile).WaitForExit();
                    ServerPickerData.Load();
                }
            }
            else
            {
                CustomServerInfo server = ServerPickerData.CustomServers[_currentId];
                GUILayout.Label("Name:", g());
                GUILayout.Label(server.Name, g());//server.Name = GUILayout.TextField(server.Name, g());
                GUILayout.Label("IP:", g());
                GUILayout.Label(server.Ip, g());//server.Ip = GUILayout.TextField(server.Ip, g());
                GUILayout.Label("Port:", g());
                GUILayout.Label(server.Port.ToString(), g());//string port = GUILayout.TextField(server.Port.ToString(), g());
                //if (int.TryParse(port, out int portInt))
                //    server.Port = portInt;
                if (GUILayout.Button("Delete", g()))
                {
                    ServerPickerData.CustomServers.RemoveAt(_currentId);
                    _currentId = -1;
                }
            }
            if (GUILayout.Button("OK", g()))
            {
                if (_currentId == -1)
                {
                    ServerPickerData.Save();
                    if (RegionMenu != null && RegionMenu.gameObject.activeSelf)
                    {
                        RegionMenu.OnDisable();
                        ServerPickerData.ForceReloadServers = true;
                        RegionMenu.OnEnable();
                        ServerPickerData.ForceReloadServers = false;
                    }
                    SetEnabled(false);
                }
                else
                    _currentId = -1;
            }
        }

        public override void Update()
        {
        }
    }
}