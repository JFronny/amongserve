﻿using BepInEx.Configuration;

namespace AmongServe.WindowSystem
{
    public class InitProvider
    {
        private readonly string _name;
        private readonly ConfigFile _config;
        public InitProvider(string name, ConfigFile config)
        {
            _name = name;
            _config = config;
        }

        public ConfigEntry<T> RegisterConfig<T>(string name, T def) => _config.Bind(_name, name.TrimStart('_'), def);
    }
}
