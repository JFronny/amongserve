﻿using BepInEx.Configuration;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongServe.WindowSystem
{
    public static class DrawingProvider
    {
        public static void RenderBox(this ConfigEntry<bool> config, string text) => config.Value = config.Value.RenderBox(text);
        public static bool RenderBox(this bool b, string text) => GUILayout.Toggle(b, text, new Il2CppReferenceArray<GUILayoutOption>(0));
    }
}