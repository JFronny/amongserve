﻿using System;
using BepInEx.Configuration;
using Reactor.Extensions;
using UnhollowerBaseLib;
using UnityEngine;

namespace AmongServe.WindowSystem
{
    public abstract class Window
    {
        private DragWindow _window;
        public abstract string GetName();
        protected abstract void Setup(InitProvider init);
        private ConfigEntry<float> _windowX;
        private ConfigEntry<float> _windowY;
        private ConfigEntry<bool> _windowExpanded;
        private static readonly string DropdownSegmentEq = new string('=', 15);
        private static readonly string DropdownDown = DropdownSegmentEq + "\\/\\/" + DropdownSegmentEq;
        private static readonly string DropdownUp = DropdownSegmentEq + "/\\/\\" + DropdownSegmentEq;
        public void Register(InitProvider init)
        {
            _windowX = init.RegisterConfig(nameof(_windowX), 20f);
            _windowY = init.RegisterConfig(nameof(_windowY), 20f);
            _windowExpanded = init.RegisterConfig(nameof(_windowExpanded), true);
            _window = new DragWindow(new Rect(_windowX.Value, _windowY.Value, 0, 0), GetName(), () =>
            {
                _windowExpanded.RenderBox(_windowExpanded.Value ? DropdownUp : DropdownDown);
                if (_windowExpanded.Value)
                    try
                    {
                        DrawContent(() => new Il2CppReferenceArray<GUILayoutOption>(0));
                    }
                    catch (Exception e)
                    {
                        AmongServe.Logger.LogError(e);
                        GUILayout.Label(e.ToString(), new Il2CppReferenceArray<GUILayoutOption>(0));
                    }
                _windowX.Value = _window.Rect.x;
                _windowY.Value = _window.Rect.y;
            });
            Setup(init);
            AmongServe.Windows.Add(this);
        }
        public void Draw()
        {
            _window.OnGUI();
        }
        public abstract void DrawContent(Func<Il2CppReferenceArray<GUILayoutOption>> g);
        public abstract void Update();
        public void SetEnabled(bool enabled)
        {
            _window.Enabled = enabled;
        }
    }
}