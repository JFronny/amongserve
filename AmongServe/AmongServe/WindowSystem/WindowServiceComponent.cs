﻿using System;
using Reactor;
using UnityEngine;

namespace AmongServe.WindowSystem
{
    [RegisterInIl2Cpp]
    public class WindowServiceComponent : MonoBehaviour
    {
        public WindowServiceComponent(IntPtr ptr) : base(ptr)
        {
        }

        private void Update()
        {
            foreach (Window window in AmongServe.Windows)
                try
                {
                    window.Update();
                }
                catch (Exception e)
                {
                    AmongServe.Logger.LogError(e);
                }
        }

        private void OnGUI()
        {
            foreach (Window window in AmongServe.Windows) window.Draw();
        }
    }
}