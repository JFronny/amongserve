﻿using Il2CppSystem.Collections.Generic;
using UnhollowerBaseLib;

namespace AmongServe
{
    public static class Extensions
    {
        public static Il2CppReferenceArray<T> GetReferenceArray<T>(this List<T> source) where T : Il2CppObjectBase
        {
            Il2CppReferenceArray<T> result = new Il2CppReferenceArray<T>(source.Count);
            for (int i = 0; i < source.Count; i++) result[i] = source[i];
            return result;
        }
    }
}