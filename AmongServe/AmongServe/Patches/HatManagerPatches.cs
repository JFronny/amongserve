﻿using HarmonyLib;
using UnhollowerBaseLib;

namespace AmongServe
{
    [HarmonyPatch(typeof(HatManager), nameof(HatManager.GetUnlockedHats))]
    public static class HatManagerHats
    {
        public static Il2CppReferenceArray<HatBehaviour> Postfix(Il2CppReferenceArray<HatBehaviour> values, HatManager __instance) => __instance.AllHats.GetReferenceArray();
    }

    [HarmonyPatch(typeof(HatManager), nameof(HatManager.GetUnlockedPets))]
    public static class HatManagerPets
    {
        public static Il2CppReferenceArray<PetBehaviour> Postfix(Il2CppReferenceArray<PetBehaviour> values, HatManager __instance) => __instance.AllPets.GetReferenceArray();
    }

    [HarmonyPatch(typeof(HatManager), nameof(HatManager.GetUnlockedSkins))]
    public static class HatManagerSkins
    {
        public static Il2CppReferenceArray<SkinData> Postfix(Il2CppReferenceArray<SkinData> values, HatManager __instance) => __instance.AllSkins.GetReferenceArray();
    }
}