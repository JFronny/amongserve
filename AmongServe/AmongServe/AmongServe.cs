﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmongServe.ServerPicker;
using AmongServe.WindowSystem;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.IL2CPP;
using BepInEx.Logging;
using HarmonyLib;
using Reactor;
using Reactor.Extensions;
using Reactor.Net;
using Reactor.Patches;
using UnityEngine;
using Window = AmongServe.WindowSystem.Window;

namespace AmongServe
{
    [BepInPlugin(Id)]
    [BepInProcess("Among Us.exe")]
    [BepInDependency(ReactorPlugin.Id)]
    [ReactorPluginSide(PluginSide.ClientOnly)]
    public class AmongServe : BasePlugin
    {
        private const string Id = "io.gitlab.jfronny.AmongServe";
        internal static readonly List<Window> Windows = new List<Window>();
        private Harmony Harmony { get; } = new Harmony(Id);
        public static ConfigEntry<bool> VanillaNetwork;

        internal static ManualLogSource Logger;
        //public static ServerManagementWindow ServerManagementWindow = new ServerManagementWindow();

        public override void Load()
        {
            RegisterInIl2CppAttribute.Register();
            Logger = Log;
            AppendVersion(":\n");
            AppendVersion(() => ModList.GetCurrent().Select(s => s.ToString()).Join(delimiter:"\n"));
            VanillaNetwork = Config.Bind("General", nameof(VanillaNetwork), true);
            GameObject go = new GameObject(nameof(AmongServe)).DontDestroy();
            go.AddComponent<WindowServiceComponent>();
            //ServerManagementWindow.Register(new InitProvider(nameof(ServerManagementWindow), Config));
            try
            {
                Harmony.PatchAll();
            }
            catch (Exception e)
            {
                AppendVersion("\n");
                AppendVersion(e.ToString());
            }
        }
    
        private void AppendVersion(Func<string> text) => ReactorVersionShower.TextUpdated += _ => ReactorVersionShower.Text.Text += text();
        private void AppendVersion(string text) => ReactorVersionShower.TextUpdated += _ => ReactorVersionShower.Text.Text += text;
    }
}
